FROM openjdk:13-alpine
RUN apk update; apk --no-cache add curl
LABEL maintainer="Maintainer"
VOLUME /tmp
ARG JAR_FILE=build/libs/lottery-1.0.0.jar
ADD ${JAR_FILE} lottery-1.0.0.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/lottery-1.0.0.jar"]
