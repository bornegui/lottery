package com.example.lottery.core;

import com.example.lottery.domain.Ticket;
import com.example.lottery.domain.TicketStatus;

public interface TicketStatusCheck {

    TicketStatus performStatusCheck(Ticket ticket);
}
