package com.example.lottery.core;

import com.example.lottery.configuration.LotteryConfiguration;
import com.example.lottery.domain.Ticket;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@AllArgsConstructor
public class TicketOperationImpl implements TicketOperation {

    private final LotteryConfiguration configuration;
    private final Random random;

    /**
     * Create a ticket.
     *
     * @param linesNumber the number of lines to create to ticket with.
     * @return the created ticket.
     */
    @Override
    public Ticket createTicket(int linesNumber) {
        List<List<Integer>> ticketLines = generateTicketLines(linesNumber);
        return Ticket.builder()
                .lines(ticketLines)
                .id(UUID.randomUUID())
                .statusChecked(false)
                .build();
    }

    /**
     * Amend a ticket.
     *
     * @param linesNumber the number of lines to amend to ticket with.
     * @param ticket      the ticket to amend.
     * @return the amended ticket.
     */
    @Override
    public void amendTicket(int linesNumber, Ticket ticket) {
        List<List<Integer>> ticketLines = generateTicketLines(linesNumber);
        ticket.getLines().addAll(ticketLines);
    }

    private List<List<Integer>> generateTicketLines(int linesNumber) {
        return IntStream.range(0, linesNumber)
                .mapToObj(i -> generateTicketLineNumbers()).collect(Collectors.toList());
    }

    private List<Integer> generateTicketLineNumbers() {
        IntStream stream = random.ints(configuration.getTicketLineNumbers(),
                configuration.getStartRange(),
                configuration.getEndRange() + 1).sorted();
        return stream
                .boxed()
                .collect(Collectors.toList());
    }
}
