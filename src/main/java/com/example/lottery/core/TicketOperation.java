package com.example.lottery.core;

import com.example.lottery.domain.Ticket;

public interface TicketOperation {

    Ticket createTicket(int linesNumber);

    void amendTicket(int linesNumber, Ticket ticket);
}
