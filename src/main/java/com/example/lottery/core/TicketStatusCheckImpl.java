package com.example.lottery.core;

import com.example.lottery.configuration.LotteryConfiguration;
import com.example.lottery.domain.Ticket;
import com.example.lottery.domain.TicketStatus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TicketStatusCheckImpl implements TicketStatusCheck {

    private final LotteryConfiguration configuration;

    /**
     * Perform ticket status check.
     *
     * @param ticket the ticket to perform the status check on.
     * @return
     */
    @Override
    public TicketStatus performStatusCheck(Ticket ticket) {
        List<Integer> status = ticket.getLines().stream().map(line -> {
            int sum = line.stream().mapToInt(Integer::intValue)
                    .sum();
            if (sum == configuration.getMaximumScoreValue()) {
                return configuration.getMaximumScore();
            }
            long distinctElements = line.stream()
                    .distinct()
                    .count();
            if (distinctElements == configuration.getMaximumDistinctElements()) {
                return configuration.getMiddleScore();
            }
            if (!line.get(1).equals(line.get(0))
                    && !line.get(2).equals(line.get(0))) {
                return configuration.getMinimumScore();
            }
            return configuration.getDefaultScore();
        }).collect(Collectors.toList());
        return TicketStatus.builder()
                .total(status.stream().mapToInt(Integer::intValue)
                        .sum())
                .status(status)
                .ticketId(ticket.getId())
                .build();
    }
}
