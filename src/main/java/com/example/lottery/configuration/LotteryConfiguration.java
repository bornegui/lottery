package com.example.lottery.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "lottery")
public class LotteryConfiguration {

    private int startRange;
    private int endRange;
    private int ticketLineNumbers;
    private int minLineNumber;
    private int maxLineNumber;
    private int maximumScore;
    private int minimumScore;
    private int middleScore;
    private int defaultScore;
    private int maximumScoreValue;
    private int maximumDistinctElements;
}
