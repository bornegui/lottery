package com.example.lottery.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.SecureRandom;

@Configuration
public class TicketConfiguration {

    @Bean
    public SecureRandom getRandom() {
        return new SecureRandom();
    }
}
