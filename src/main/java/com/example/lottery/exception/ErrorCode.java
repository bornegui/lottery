package com.example.lottery.exception;

public enum ErrorCode {

    INVALID_ARGUMENT_EXCEPTION(1000),

    RESOURCE_NOT_FOUND_EXCEPTION(1001),

    NOT_AUTHORIZED_OPERATION_EXCEPTION(1002);

    private final int code;

    ErrorCode(final int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
