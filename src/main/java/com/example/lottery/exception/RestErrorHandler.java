package com.example.lottery.exception;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class RestErrorHandler implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        return body;
    }

    @ExceptionHandler(InvalidArgumentException.class)
    public ApiError invalidArgumentException(HttpServletRequest request, Exception ex) {
        return ApiError.builder()
                .code(ErrorCode.INVALID_ARGUMENT_EXCEPTION.getCode())
                .message(ex.getLocalizedMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .build();
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ApiError resourceNotFoundException(HttpServletRequest request, Exception ex) {
        return ApiError.builder()
                .code(ErrorCode.RESOURCE_NOT_FOUND_EXCEPTION.getCode())
                .message(ex.getLocalizedMessage())
                .status(HttpStatus.NOT_FOUND.value())
                .build();
    }

    @ExceptionHandler(NotAuthorizedOperationException.class)
    public ApiError notAuthorizedOperationException(HttpServletRequest request, Exception ex) {
        return ApiError.builder()
                .code(ErrorCode.NOT_AUTHORIZED_OPERATION_EXCEPTION.getCode())
                .message(ex.getLocalizedMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .build();
    }
}
