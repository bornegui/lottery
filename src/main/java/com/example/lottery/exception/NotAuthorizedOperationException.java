package com.example.lottery.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NotAuthorizedOperationException extends RuntimeException {

    public NotAuthorizedOperationException(String message) {
        super(message);
    }
}
