package com.example.lottery.application.service;

import com.example.lottery.application.port.in.AmendTicketUseCase;
import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.application.port.out.UpdateTicketPort;
import com.example.lottery.core.TicketOperation;
import com.example.lottery.domain.Ticket;
import com.example.lottery.exception.NotAuthorizedOperationException;
import com.example.lottery.exception.ResourceNotFoundException;
import com.example.lottery.validators.TicketValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AmendTicketService implements AmendTicketUseCase {

    private final UpdateTicketPort updateTicketPort;
    private final GetTicketPort getTicketPort;
    private final TicketValidator validator;
    private final TicketOperation ticketOperation;

    /**
     * Amend a ticket.
     *
     * @param id          the ticket id.
     * @param linesNumber the number of lines to amend to ticket with.
     * @return the amended ticket.
     */
    @Override
    public Ticket amendTicket(UUID id, int linesNumber) {
        Optional<Ticket> ticketOpt = getTicketPort.getTicket(id);
        if (!ticketOpt.isPresent()) {
            throw new ResourceNotFoundException(String.format("Resource with ID %s was not found", id));
        }
        Ticket ticket = ticketOpt.get();
        if (ticket.isStatusChecked()) {
            throw new NotAuthorizedOperationException("Not allowed to amend an already checked ticket");
        }
        validator.validate(linesNumber + ticket.getLines().size());
        ticketOperation.amendTicket(linesNumber, ticket);
        updateTicketPort.updateTicket(ticket);
        return ticket;
    }
}
