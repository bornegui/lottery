package com.example.lottery.application.service;

import com.example.lottery.application.port.in.CheckTicketStatusUseCase;
import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.application.port.out.UpdateTicketPort;
import com.example.lottery.core.TicketStatusCheck;
import com.example.lottery.domain.Ticket;
import com.example.lottery.domain.TicketStatus;
import com.example.lottery.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class CheckTicketStatusService implements CheckTicketStatusUseCase {

    private final GetTicketPort getTicketPort;
    private final TicketStatusCheck ticketStatusCheck;
    private final UpdateTicketPort updateTicketPort;

    /**
     * Check a ticket status.
     *
     * @param id the ticket id.
     * @return the ticket status.
     */
    @Override
    public TicketStatus checkTicketStatus(UUID id) {
        Optional<Ticket> ticketOpt = getTicketPort.getTicket(id);
        if (!ticketOpt.isPresent()) {
            throw new ResourceNotFoundException(String.format("Resource with ID %s was not found", id));
        }
        Ticket ticket = ticketOpt.get();
        TicketStatus status = ticketStatusCheck.performStatusCheck(ticket);
        ticket.setStatusChecked(true);
        updateTicketPort.updateTicket(ticket);
        return status;
    }
}
