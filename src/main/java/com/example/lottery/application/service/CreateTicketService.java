package com.example.lottery.application.service;

import com.example.lottery.application.port.in.CreateTicketUseCase;
import com.example.lottery.application.port.out.SaveTicketPort;
import com.example.lottery.core.TicketOperation;
import com.example.lottery.domain.Ticket;
import com.example.lottery.validators.TicketValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CreateTicketService implements CreateTicketUseCase {

    private final SaveTicketPort saveTicketPort;
    private final TicketValidator validator;
    private final TicketOperation ticketOperation;

    /**
     * Create a ticket.
     *
     * @param linesNumber the number of lines to create to ticket with.
     * @return the created ticket.
     */
    @Override
    public Ticket createTicket(int linesNumber) {
        validator.validate(linesNumber);
        Ticket ticket = ticketOperation.createTicket(linesNumber);
        saveTicketPort.saveTicket(ticket);
        return ticket;
    }
}
