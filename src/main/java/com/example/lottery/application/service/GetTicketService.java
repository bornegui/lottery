package com.example.lottery.application.service;

import com.example.lottery.application.port.in.GetTicketUseCase;
import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.domain.Ticket;
import com.example.lottery.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class GetTicketService implements GetTicketUseCase {

    private final GetTicketPort getTicketPort;

    /**
     * Get a ticket.
     *
     * @param id the ticket id.
     * @return the ticket.
     */
    @Override
    public Ticket getTicket(UUID id) {
        Optional<Ticket> ticket = getTicketPort.getTicket(id);
        if (!ticket.isPresent()) {
            throw new ResourceNotFoundException(String.format("Resource with ID %s was not found", id));
        }
        return ticket.get();
    }
}
