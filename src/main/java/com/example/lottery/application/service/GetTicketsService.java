package com.example.lottery.application.service;

import com.example.lottery.application.port.in.GetTicketsUseCase;
import com.example.lottery.application.port.out.GetTicketsPort;
import com.example.lottery.domain.Ticket;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@AllArgsConstructor
public class GetTicketsService implements GetTicketsUseCase {

    private final GetTicketsPort getTicketsPort;

    /**
     * Get all the tickets.
     *
     * @return the tickets.
     */
    @Override
    public Set<Ticket> getTickets() {
        return getTicketsPort.getTickets();
    }
}
