package com.example.lottery.application.port.out;

import com.example.lottery.domain.Ticket;

public interface SaveTicketPort {

    void saveTicket(Ticket ticket);
}
