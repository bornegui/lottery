package com.example.lottery.application.port.out;

import com.example.lottery.domain.Ticket;

import java.util.Set;

public interface GetTicketsPort {

    Set<Ticket> getTickets();
}
