package com.example.lottery.application.port.in;

import com.example.lottery.domain.TicketStatus;

import java.util.UUID;

public interface CheckTicketStatusUseCase {

    TicketStatus checkTicketStatus(UUID id);
}
