package com.example.lottery.application.port.in;

import com.example.lottery.domain.Ticket;

import java.util.Set;

public interface GetTicketsUseCase {

    Set<Ticket> getTickets();
}
