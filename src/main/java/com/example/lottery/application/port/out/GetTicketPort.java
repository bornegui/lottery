package com.example.lottery.application.port.out;

import com.example.lottery.domain.Ticket;

import java.util.Optional;
import java.util.UUID;

public interface GetTicketPort {

    Optional<Ticket> getTicket(UUID id);
}
