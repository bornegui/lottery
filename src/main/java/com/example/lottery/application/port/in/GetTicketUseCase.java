package com.example.lottery.application.port.in;

import com.example.lottery.domain.Ticket;

import java.util.UUID;

public interface GetTicketUseCase {

    Ticket getTicket(UUID id);
}
