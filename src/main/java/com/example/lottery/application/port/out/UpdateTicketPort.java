package com.example.lottery.application.port.out;

import com.example.lottery.domain.Ticket;

public interface UpdateTicketPort {

    void updateTicket(Ticket ticket);
}
