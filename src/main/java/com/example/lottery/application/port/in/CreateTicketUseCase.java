package com.example.lottery.application.port.in;

import com.example.lottery.domain.Ticket;

public interface CreateTicketUseCase {

    Ticket createTicket(int linesNumber);
}
