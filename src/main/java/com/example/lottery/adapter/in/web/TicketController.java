package com.example.lottery.adapter.in.web;

import com.example.lottery.application.port.in.*;
import com.example.lottery.domain.Ticket;
import com.example.lottery.domain.TicketStatus;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/tickets")
@AllArgsConstructor
public class TicketController {

    private final CreateTicketUseCase createTicketUseCase;
    private final GetTicketUseCase getTicketUseCase;
    private final GetTicketsUseCase getTicketsUseCase;
    private final AmendTicketUseCase amendTicketUseCase;
    private final CheckTicketStatusUseCase checkTicketStatusUseCase;

    @PostMapping
    public ResponseEntity<Ticket> createTicket(@RequestParam int linesNumber) {
        Ticket ticket = createTicketUseCase.createTicket(linesNumber);
        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Ticket> getTicket(@PathVariable UUID id) {
        Ticket ticket = getTicketUseCase.getTicket(id);
        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Set<Ticket>> getTickets() {
        Set<Ticket> tickets = getTicketsUseCase.getTickets();
        if (tickets.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Ticket> amendTicket(@PathVariable UUID id, @RequestParam int linesNumber) {
        Ticket ticket = amendTicketUseCase.amendTicket(id, linesNumber);
        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

    @GetMapping("/{id}/status")
    public ResponseEntity<TicketStatus> checkTicketStatus(@PathVariable UUID id) {
        TicketStatus status = checkTicketStatusUseCase.checkTicketStatus(id);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }
}
