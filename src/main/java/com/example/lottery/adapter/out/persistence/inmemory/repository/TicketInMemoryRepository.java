package com.example.lottery.adapter.out.persistence.inmemory.repository;

import com.example.lottery.adapter.out.persistence.inmemory.model.TicketInMemoryModel;
import com.example.lottery.domain.Ticket;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Component
public class TicketInMemoryRepository {

    /**
     * Create or update a ticket.
     *
     * @param ticket the ticket to create or to update.
     * @return
     */
    public Ticket createOrUpdateTicket(Ticket ticket) {
        TicketInMemoryModel.getTickets().add(ticket);
        return ticket;
    }

    /**
     * Get a ticket.
     *
     * @param id the ticket id.
     * @return the ticket.
     */
    public Optional<Ticket> getTicket(UUID id) {
        return TicketInMemoryModel.getTickets().stream().filter(ticket -> ticket.getId().equals(id)).findAny();
    }

    /**
     * Get all the tickets.
     *
     * @return the tickets.
     */
    public Set<Ticket> getTickets() {
        return TicketInMemoryModel.getTickets();
    }
}
