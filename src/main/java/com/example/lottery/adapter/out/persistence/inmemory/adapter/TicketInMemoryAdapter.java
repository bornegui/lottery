package com.example.lottery.adapter.out.persistence.inmemory.adapter;

import com.example.lottery.adapter.out.persistence.inmemory.repository.TicketInMemoryRepository;
import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.application.port.out.GetTicketsPort;
import com.example.lottery.application.port.out.SaveTicketPort;
import com.example.lottery.application.port.out.UpdateTicketPort;
import com.example.lottery.domain.Ticket;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Component
@AllArgsConstructor
public class TicketInMemoryAdapter implements SaveTicketPort,
        GetTicketPort,
        GetTicketsPort,
        UpdateTicketPort {

    private final TicketInMemoryRepository repository;

    /**
     * Create a ticket.
     *
     * @param ticket the ticket to create.
     */
    @Override
    public void saveTicket(Ticket ticket) {
        repository.createOrUpdateTicket(ticket);
    }

    /**
     * Get a ticket.
     *
     * @param id the ticket id.
     * @return the ticket.
     */
    @Override
    public Optional<Ticket> getTicket(UUID id) {
        return repository.getTicket(id);
    }

    /**
     * Get all the tickets.
     *
     * @return the tickets.
     */
    @Override
    public Set<Ticket> getTickets() {
        return repository.getTickets();
    }

    /**
     * Update a ticket.
     *
     * @param ticket the ticket to update.
     */
    @Override
    public void updateTicket(Ticket ticket) {
        repository.createOrUpdateTicket(ticket);
    }
}
