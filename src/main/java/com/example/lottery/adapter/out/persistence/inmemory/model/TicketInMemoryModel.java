package com.example.lottery.adapter.out.persistence.inmemory.model;

import com.example.lottery.domain.Ticket;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

public class TicketInMemoryModel {

    @Getter
    private static final Set<Ticket> tickets = new HashSet<>();

    private TicketInMemoryModel() {
        throw new IllegalStateException("Utility class");
    }
}
