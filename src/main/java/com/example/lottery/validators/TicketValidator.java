package com.example.lottery.validators;

import com.example.lottery.configuration.LotteryConfiguration;
import com.example.lottery.exception.InvalidArgumentException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TicketValidator {

    private final LotteryConfiguration configuration;

    /**
     * Validate number of lines parameter.
     *
     * @param linesNumber the number of lines to check.
     */
    public void validate(int linesNumber) {
        int minLineNumber = configuration.getMinLineNumber();
        int maxLineNumber = configuration.getMaxLineNumber();
        if (linesNumber < minLineNumber
                || linesNumber > maxLineNumber) {
            throw new InvalidArgumentException(String.format("Number of lines must be between %s and %s",
                    minLineNumber, maxLineNumber));
        }
    }
}
