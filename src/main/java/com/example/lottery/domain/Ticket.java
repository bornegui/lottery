package com.example.lottery.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Ticket {

    UUID id;
    List<List<Integer>> lines;
    boolean statusChecked;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Ticket))
            return false;
        if (obj == this) {
            return true;
        }
        return this.getId().equals(((Ticket) obj).getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }
}
