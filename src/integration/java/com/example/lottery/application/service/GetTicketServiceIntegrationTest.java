package com.example.lottery.application.service;

import com.example.lottery.adapter.out.persistence.inmemory.model.TicketInMemoryModel;
import com.example.lottery.application.service.GetTicketService;
import com.example.lottery.domain.Ticket;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetTicketServiceIntegrationTest {

    @Autowired
    private GetTicketService getTicketService;

    @BeforeAll
    public static void setUp() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder().id(uuid).build();
        Set<Ticket> tickets = TicketInMemoryModel.getTickets();
        tickets.clear();
        tickets.add(mockedTicket);
    }

    @Test
    void test_getTicket_withValidId_shoudGetTicket() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket ticket = getTicketService.getTicket(uuid);
        assertThat(ticket).isNotNull();
    }
}
