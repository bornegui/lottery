package com.example.lottery.application.service;

import com.example.lottery.adapter.out.persistence.inmemory.model.TicketInMemoryModel;
import com.example.lottery.application.service.GetTicketsService;
import com.example.lottery.domain.Ticket;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetTicketsServiceIntegrationTest {

    @Autowired
    private GetTicketsService getTicketsService;

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestNotEmptyTickets {

        @BeforeAll
        public void setUp() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            Ticket mockedTicket = Ticket.builder().id(uuid).build();
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
            tickets.add(mockedTicket);
        }

        @Test
        void test_getTickets_withNotEmptyTickets_shoudGetTickets() {
            Set<Ticket> tickets = getTicketsService.getTickets();
            assertThat(tickets).isNotEmpty();
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestEmptyTickets {

        @BeforeAll
        public void setUp() {
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
        }

        @Test
        void test_getTickets_withEmptyTickets_shoudGetNoTickets() {
            Set<Ticket> tickets = getTicketsService.getTickets();
            assertThat(tickets).isEmpty();
        }
    }
}
