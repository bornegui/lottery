package com.example.lottery.application.service;

import com.example.lottery.application.service.CreateTicketService;
import com.example.lottery.domain.Ticket;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CreateTicketServiceIntegrationTest {

    @Autowired
    private CreateTicketService createTicketService;

    @Test
    void test_createTicket_withValidLinesNumber_shoudCreateTicket() {
        Ticket ticket = createTicketService.createTicket(3);
        assertThat(ticket).isNotNull();
        assertThat(ticket.getLines().size()).isEqualTo(3);
    }
}
