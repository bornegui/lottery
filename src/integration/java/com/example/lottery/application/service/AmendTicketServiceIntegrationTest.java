package com.example.lottery.application.service;

import com.example.lottery.adapter.out.persistence.inmemory.model.TicketInMemoryModel;
import com.example.lottery.domain.Ticket;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AmendTicketServiceIntegrationTest {

    @Autowired
    private AmendTicketService amendTicketService;

    @BeforeAll
    public static void setUp() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 2)))))
                .build();
        Set<Ticket> tickets = TicketInMemoryModel.getTickets();
        tickets.clear();
        tickets.add(mockedTicket);
    }

    @Test
    void test_amendTicket_withValidLinesNumberAndValidTicket_shoudAmendTicket() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket ticket = amendTicketService.amendTicket(uuid, 3);
        assertThat(ticket).isNotNull();
        assertThat(ticket.getLines().size()).isEqualTo(4);
    }
}
