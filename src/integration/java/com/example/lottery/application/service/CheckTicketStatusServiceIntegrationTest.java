package com.example.lottery.application.service;

import com.example.lottery.adapter.out.persistence.inmemory.model.TicketInMemoryModel;
import com.example.lottery.domain.Ticket;
import com.example.lottery.domain.TicketStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CheckTicketStatusServiceIntegrationTest {

    @Autowired
    private CheckTicketStatusService checkTicketStatusService;

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestScore10 {

        @BeforeAll
        public void setUp() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            Ticket mockedTicket = Ticket.builder()
                    .id(uuid)
                    .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 1)))))
                    .build();
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
            tickets.add(mockedTicket);
        }

        @Test
        void test_performStatusCheck_withLineTicketSumEqualsTo2_shoudReturn10() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
            assertThat(status.getStatus().get(0)).isEqualTo(10);
            assertThat(status.getTotal()).isEqualTo(10);
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestScore5 {

        @BeforeAll
        public void setUp() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            Ticket mockedTicket = Ticket.builder()
                    .id(uuid)
                    .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(1, 1, 1)))))
                    .build();
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
            tickets.add(mockedTicket);
        }

        @Test
        void test_performStatusCheck_withLineTicketSameNumbers_shoudReturn5() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
            assertThat(status.getStatus().get(0)).isEqualTo(5);
            assertThat(status.getTotal()).isEqualTo(5);
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestScore1 {

        @BeforeAll
        public void setUp() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            Ticket mockedTicket = Ticket.builder()
                    .id(uuid)
                    .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 2)))))
                    .build();
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
            tickets.add(mockedTicket);
        }

        @Test
        void test_performStatusCheck_withLineTicket2and3NumbersNotEqualTo1_shoudReturn1() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
            assertThat(status.getStatus().get(0)).isEqualTo(1);
            assertThat(status.getTotal()).isEqualTo(1);
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestScore0 {

        @BeforeAll
        public void setUp() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            Ticket mockedTicket = Ticket.builder()
                    .id(uuid)
                    .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(1, 1, 2)))))
                    .build();
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
            tickets.add(mockedTicket);
        }

        @Test
        void test_performStatusCheck_withNoSpecialLineTicketScore_shoudReturn0() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
            assertThat(status.getStatus().get(0)).isZero();
            assertThat(status.getTotal()).isZero();
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class TestTotal {

        @BeforeAll
        public void setUp() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            List<Integer> line1 = Arrays.asList(0, 1, 2);
            List<Integer> line2 = Arrays.asList(1, 1, 1);
            List<Integer> line3 = Arrays.asList(0, 1, 1);
            Ticket mockedTicket = Ticket.builder()
                    .id(uuid)
                    .lines(new ArrayList<>(Arrays.asList(line1, line2, line3)))
                    .build();
            Set<Ticket> tickets = TicketInMemoryModel.getTickets();
            tickets.clear();
            tickets.add(mockedTicket);
        }

        @Test
        void test_performStatusCheck_withMultipleLinesTicket_shoudReturnGoodTotal() {
            UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
            TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
            assertThat(status.getTotal()).isEqualTo(16);
        }
    }
}
