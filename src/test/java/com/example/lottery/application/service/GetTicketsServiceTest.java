package com.example.lottery.application.service;

import com.example.lottery.application.port.out.GetTicketsPort;
import com.example.lottery.application.service.GetTicketsService;
import com.example.lottery.domain.Ticket;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
class GetTicketsServiceTest {

    @Mock
    private GetTicketsPort getTicketsPort;
    private GetTicketsService getTicketsService;

    @BeforeEach
    public void setUp() {
        getTicketsService = new GetTicketsService(getTicketsPort);
    }

    @Test
    void test_getTickets_withNotEmptyTickets_shoudGetTickets() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder().id(uuid).build();
        when(getTicketsPort.getTickets()).thenReturn(Set.of(mockedTicket));
        Set<Ticket> tickets = getTicketsService.getTickets();
        assertThat(tickets).isNotEmpty();
    }

    @Test
    void test_getTickets_withEmptyTickets_shoudGetNoTickets() {
        when(getTicketsPort.getTickets()).thenReturn(new HashSet<>());
        Set<Ticket> tickets = getTicketsService.getTickets();
        assertThat(tickets).isEmpty();
    }
}
