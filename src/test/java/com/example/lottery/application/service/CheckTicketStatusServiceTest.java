package com.example.lottery.application.service;

import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.application.port.out.UpdateTicketPort;
import com.example.lottery.application.service.CheckTicketStatusService;
import com.example.lottery.core.TicketStatusCheck;
import com.example.lottery.domain.Ticket;
import com.example.lottery.domain.TicketStatus;
import com.example.lottery.exception.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
class CheckTicketStatusServiceTest {

    @Mock
    private UpdateTicketPort updateTicketPort;
    @Mock
    private GetTicketPort getTicketPort;
    private CheckTicketStatusService checkTicketStatusService;
    @Autowired
    private TicketStatusCheck ticketStatusCheck;

    @BeforeEach
    public void setUp() {
        checkTicketStatusService = new CheckTicketStatusService(getTicketPort,
                ticketStatusCheck,
                updateTicketPort);
    }

    @Test
    void test_performStatusCheck_withLineTicketSumEqualsTo2_shoudReturn10() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 1)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
        assertThat(status.getStatus().get(0)).isEqualTo(10);
        assertThat(status.getTotal()).isEqualTo(10);
    }

    @Test
    void test_performStatusCheck_withLineTicketSameNumbers_shoudReturn5() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(1, 1, 1)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
        assertThat(status.getStatus().get(0)).isEqualTo(5);
        assertThat(status.getTotal()).isEqualTo(5);
    }

    @Test
    void test_performStatusCheck_withLineTicket2and3NumbersNotEqualTo1_shoudReturn1() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 2)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
        assertThat(status.getStatus().get(0)).isEqualTo(1);
        assertThat(status.getTotal()).isEqualTo(1);
    }

    @Test
    void test_performStatusCheck_withNoSpecialLineTicketScore_shoudReturn0() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(1, 1, 2)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
        assertThat(status.getStatus().get(0)).isZero();
        assertThat(status.getTotal()).isZero();
    }

    @Test
    void test_performStatusCheck_withMultipleLinesTicket_shoudReturnGoodTotal() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        List<Integer> line1 = Arrays.asList(0, 1, 2);
        List<Integer> line2 = Arrays.asList(1, 1, 1);
        List<Integer> line3 = Arrays.asList(0, 1, 1);
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Arrays.asList(line1, line2, line3)))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        TicketStatus status = checkTicketStatusService.checkTicketStatus(uuid);
        assertThat(status.getTotal()).isEqualTo(16);
    }

    @Test
    void test_performStatusCheck_withInvalidTicket_shoudThrowException() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> checkTicketStatusService.checkTicketStatus(uuid));
    }
}
