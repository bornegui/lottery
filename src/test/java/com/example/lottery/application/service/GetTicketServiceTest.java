package com.example.lottery.application.service;

import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.application.service.GetTicketService;
import com.example.lottery.domain.Ticket;
import com.example.lottery.exception.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
class GetTicketServiceTest {

    @Mock
    private GetTicketPort getTicketPort;
    private GetTicketService getTicketService;

    @BeforeEach
    public void setUp() {
        getTicketService = new GetTicketService(getTicketPort);
    }

    @Test
    void test_getTicket_withValidId_shoudGetTicket() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder().id(uuid).build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        Ticket ticket = getTicketService.getTicket(uuid);
        assertThat(ticket).isNotNull();
    }

    @Test
    void test_getTicket_withInvalidId_shoudThrowAnException() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        assertThrows(ResourceNotFoundException.class, () -> getTicketService.getTicket(uuid));
    }
}
