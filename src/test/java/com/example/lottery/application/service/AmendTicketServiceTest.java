package com.example.lottery.application.service;

import com.example.lottery.application.port.out.GetTicketPort;
import com.example.lottery.application.port.out.UpdateTicketPort;
import com.example.lottery.application.service.AmendTicketService;
import com.example.lottery.core.TicketOperation;
import com.example.lottery.domain.Ticket;
import com.example.lottery.exception.InvalidArgumentException;
import com.example.lottery.exception.NotAuthorizedOperationException;
import com.example.lottery.exception.ResourceNotFoundException;
import com.example.lottery.validators.TicketValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
class AmendTicketServiceTest {

    @Mock
    private UpdateTicketPort updateTicketPort;
    @Mock
    private GetTicketPort getTicketPort;
    private AmendTicketService amendTicketService;
    @Autowired
    private TicketOperation ticketOperation;

    @Autowired
    private TicketValidator validator;

    @BeforeEach
    public void setUp() {
        amendTicketService = new AmendTicketService(updateTicketPort,
                getTicketPort,
                validator,
                ticketOperation);
    }

    @Test
    void test_amendTicket_withValidLinesNumberAndValidTicket_shoudAmendTicket() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 2)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        Ticket ticket = amendTicketService.amendTicket(uuid, 3);
        assertThat(ticket).isNotNull();
        assertThat(ticket.getLines().size()).isEqualTo(4);
    }

    @Test
    void test_amendTicket_withInvalidLinesNumberAndValidTicket_shoudThrowException() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 2)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        assertThrows(InvalidArgumentException.class, () -> amendTicketService.amendTicket(uuid, 10));
    }

    @Test
    void test_amendTicket_withValidLinesNumberAndInvalidTicket_shoudThrowException() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        Ticket mockedTicket = Ticket.builder()
                .id(uuid)
                .statusChecked(true)
                .lines(new ArrayList<>(Collections.singleton(new ArrayList<>(Arrays.asList(0, 1, 2)))))
                .build();
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.of(mockedTicket));
        assertThrows(NotAuthorizedOperationException.class, () -> amendTicketService.amendTicket(uuid, 3));
    }

    @Test
    void test_amendTicket_withInvalidTicket_shoudThrowException() {
        UUID uuid = UUID.fromString("3e90e0a1-881c-4cad-9346-1248c34ab47b");
        when(getTicketPort.getTicket(uuid)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> amendTicketService.amendTicket(uuid, 3));
    }
}
