package com.example.lottery.application.service;

import com.example.lottery.application.port.out.SaveTicketPort;
import com.example.lottery.application.service.CreateTicketService;
import com.example.lottery.core.TicketOperation;
import com.example.lottery.domain.Ticket;
import com.example.lottery.exception.InvalidArgumentException;
import com.example.lottery.validators.TicketValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class CreateTicketServiceTest {

    @Mock
    private SaveTicketPort saveTicketPort;
    private CreateTicketService createTicketService;
    @Autowired
    private TicketOperation ticketOperation;

    @Autowired
    private TicketValidator validator;

    @BeforeEach
    public void setUp() {
        createTicketService = new CreateTicketService(saveTicketPort,
                validator,
                ticketOperation);
    }

    @Test
    void test_createTicket_withValidLinesNumber_shoudCreateTicket() {
        Ticket ticket = createTicketService.createTicket(3);
        assertThat(ticket).isNotNull();
        assertThat(ticket.getLines().size()).isEqualTo(3);
    }

    @Test
    void test_createTicket_withInvalidLinesNumber_shoudThrowAnException() {
        assertThrows(InvalidArgumentException.class, () -> createTicketService.createTicket(15));
    }
}
