## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Endpoints](#endpoints)
* [Documentation](#documentation)
* [Tests](#tests)

## General info
This Lottery API allows users to perform basic operations for a lottery game.

## Technologies
Project is created with:
* Java 11
* Gradle
* Spring Boot 2.4.2
* Lombok
* JUnit 5
* Swagger 2
* Docker
* SonarQube

## Setup
To run this project, perform the following commands:

```
$ ./gradlew clean build
$ java -jar build/libs/lottery-1.0.0.jar
```

The project can be also run using Docker:

```
$ docker build -t lottery .
$ docker run -d -p 8080:8080 lottery
```

## Endpoints
The following operations are available:
```
POST http://localhost:8080/api/v1/tickets?linesNumber={number}
GET http://localhost:8080/api/v1/tickets
GET http://localhost:8080/api/v1/tickets/{id}
GET http://localhost:8080/api/v1/tickets/{id}/status
PUT http://localhost:8080/api/v1/tickets/{id}?linesNumber={number}
```

## Documentation
Swagger 2 UI is available at the following URL:
```
http://localhost:8080/swagger-ui.html
```

## Tests
There are some unit tests and some integration tests which are located in a separated "integration" folder.
Executing the following command will run both the unit tests and the integration tests:
```
$ ./gradlew clean build
```
Executing the following command will run only the unit tests:
```
$ ./gradlew clean test
```
Executing the following command will run only the integration tests:
```
$ ./gradlew clean integrationTest
```
